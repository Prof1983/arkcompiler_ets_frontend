{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "Identifier",
                          "name": "y",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 5
                            },
                            "end": {
                              "line": 16,
                              "column": 6
                            }
                          }
                        },
                        "right": {
                          "type": "CallExpression",
                          "callee": {
                            "type": "Identifier",
                            "name": "foo",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 9
                              },
                              "end": {
                                "line": 16,
                                "column": 12
                              }
                            }
                          },
                          "arguments": [],
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 9
                            },
                            "end": {
                              "line": 16,
                              "column": 14
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 16,
                            "column": 5
                          },
                          "end": {
                            "line": 16,
                            "column": 14
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 5
                        },
                        "end": {
                          "line": 16,
                          "column": 14
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 15,
                  "column": 10
                },
                "end": {
                  "line": 15,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 10
                    },
                    "end": {
                      "line": 15,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 18
                    },
                    "end": {
                      "line": 15,
                      "column": 22
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 15,
                    "column": 13
                  },
                  "end": {
                    "line": 15,
                    "column": 14
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 15,
                  "column": 13
                },
                "end": {
                  "line": 15,
                  "column": 14
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 15,
                "column": 1
              },
              "end": {
                "line": 15,
                "column": 14
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "y",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 5
                },
                "end": {
                  "line": 16,
                  "column": 6
                }
              }
            },
            "value": {
              "type": "CallExpression",
              "callee": {
                "type": "Identifier",
                "name": "foo",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 9
                  },
                  "end": {
                    "line": 16,
                    "column": 12
                  }
                }
              },
              "arguments": [],
              "optional": false,
              "loc": {
                "start": {
                  "line": 16,
                  "column": 9
                },
                "end": {
                  "line": 16,
                  "column": 14
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 5
              },
              "end": {
                "line": 16,
                "column": 14
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 16,
      "column": 14
    }
  }
}
TypeError: Cannot use type 'void' as value.  [void_as_typeAnnotation_neg_1.ets:16:9]
