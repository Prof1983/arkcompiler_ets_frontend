{
  "type": "Program",
  "statements": [
    {
      "type": "ImportDeclaration",
      "source": {
        "type": "StringLiteral",
        "value": "./infer_imported_function_return_type_lib",
        "loc": {
          "start": {
            "line": 16,
            "column": 19
          },
          "end": {
            "line": 16,
            "column": 62
          }
        }
      },
      "specifiers": [
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "foo",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 12
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "foo",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 12
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 9
            },
            "end": {
              "line": 16,
              "column": 12
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 63
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 10
                },
                "end": {
                  "line": 18,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 10
                    },
                    "end": {
                      "line": 18,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "x",
                            "typeAnnotation": {
                              "type": "ETSUnionType",
                              "types": [
                                {
                                  "type": "ETSTypeReference",
                                  "part": {
                                    "type": "ETSTypeReferencePart",
                                    "name": {
                                      "type": "Identifier",
                                      "name": "String",
                                      "decorators": [],
                                      "loc": {
                                        "start": {
                                          "line": 19,
                                          "column": 12
                                        },
                                        "end": {
                                          "line": 19,
                                          "column": 18
                                        }
                                      }
                                    },
                                    "loc": {
                                      "start": {
                                        "line": 19,
                                        "column": 12
                                      },
                                      "end": {
                                        "line": 19,
                                        "column": 20
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 19,
                                      "column": 12
                                    },
                                    "end": {
                                      "line": 19,
                                      "column": 20
                                    }
                                  }
                                },
                                {
                                  "type": "ETSPrimitiveType",
                                  "loc": {
                                    "start": {
                                      "line": 19,
                                      "column": 21
                                    },
                                    "end": {
                                      "line": 19,
                                      "column": 24
                                    }
                                  }
                                }
                              ],
                              "loc": {
                                "start": {
                                  "line": 19,
                                  "column": 12
                                },
                                "end": {
                                  "line": 19,
                                  "column": 24
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 9
                              },
                              "end": {
                                "line": 19,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "CallExpression",
                            "callee": {
                              "type": "Identifier",
                              "name": "foo",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 19,
                                  "column": 27
                                },
                                "end": {
                                  "line": 19,
                                  "column": 30
                                }
                              }
                            },
                            "arguments": [
                              {
                                "type": "NumberLiteral",
                                "value": 1,
                                "loc": {
                                  "start": {
                                    "line": 19,
                                    "column": 31
                                  },
                                  "end": {
                                    "line": 19,
                                    "column": 32
                                  }
                                }
                              }
                            ],
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 27
                              },
                              "end": {
                                "line": 19,
                                "column": 33
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 9
                            },
                            "end": {
                              "line": 19,
                              "column": 33
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 5
                        },
                        "end": {
                          "line": 19,
                          "column": 34
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 17
                    },
                    "end": {
                      "line": 20,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 14
                  },
                  "end": {
                    "line": 20,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 14
                },
                "end": {
                  "line": 20,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 1
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 21,
      "column": 1
    }
  }
}
