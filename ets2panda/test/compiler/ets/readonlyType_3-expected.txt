{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "fld",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 17,
                  "column": 8
                }
              }
            },
            "value": {
              "type": "NumberLiteral",
              "value": 2,
              "loc": {
                "start": {
                  "line": 17,
                  "column": 19
                },
                "end": {
                  "line": 17,
                  "column": 20
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "Number",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 10
                    },
                    "end": {
                      "line": 17,
                      "column": 16
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 10
                  },
                  "end": {
                    "line": 17,
                    "column": 18
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 10
                },
                "end": {
                  "line": 17,
                  "column": 18
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 17,
                "column": 20
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 8
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 5
                    },
                    "end": {
                      "line": 18,
                      "column": 8
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a0",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Readonly",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 14
                              },
                              "end": {
                                "line": 18,
                                "column": 22
                              }
                            }
                          },
                          "typeParams": {
                            "type": "TSTypeParameterInstantiation",
                            "params": [
                              {
                                "type": "ETSTypeReference",
                                "part": {
                                  "type": "ETSTypeReferencePart",
                                  "name": {
                                    "type": "Identifier",
                                    "name": "A",
                                    "decorators": [],
                                    "loc": {
                                      "start": {
                                        "line": 18,
                                        "column": 23
                                      },
                                      "end": {
                                        "line": 18,
                                        "column": 24
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 23
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 25
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 23
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 25
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 22
                              },
                              "end": {
                                "line": 18,
                                "column": 25
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 14
                            },
                            "end": {
                              "line": 18,
                              "column": 26
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 14
                          },
                          "end": {
                            "line": 18,
                            "column": 26
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 10
                        },
                        "end": {
                          "line": 18,
                          "column": 26
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 10
                      },
                      "end": {
                        "line": 18,
                        "column": 26
                      }
                    }
                  }
                ],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "Identifier",
                            "name": "a0",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 9
                              },
                              "end": {
                                "line": 19,
                                "column": 11
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "fld",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 12
                              },
                              "end": {
                                "line": 19,
                                "column": 15
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 9
                            },
                            "end": {
                              "line": 19,
                              "column": 15
                            }
                          }
                        },
                        "right": {
                          "type": "NumberLiteral",
                          "value": 5,
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 18
                            },
                            "end": {
                              "line": 19,
                              "column": 19
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 9
                          },
                          "end": {
                            "line": 19,
                            "column": 19
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 9
                        },
                        "end": {
                          "line": 19,
                          "column": 19
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 27
                    },
                    "end": {
                      "line": 20,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 9
                  },
                  "end": {
                    "line": 20,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 9
                },
                "end": {
                  "line": 20,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 5
              },
              "end": {
                "line": 20,
                "column": 6
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 21,
                "column": 2
              },
              "end": {
                "line": 21,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 9
          },
          "end": {
            "line": 21,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 21,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 22,
      "column": 1
    }
  }
}
TypeError: Cannot assign to a readonly variable fld [readonlyType_3.ets:17:5]
