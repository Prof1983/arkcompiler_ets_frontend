{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 10
                },
                "end": {
                  "line": 16,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 10
                    },
                    "end": {
                      "line": 16,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 19
                    },
                    "end": {
                      "line": 16,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "e",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Float",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 17,
                                      "column": 12
                                    },
                                    "end": {
                                      "line": 17,
                                      "column": 17
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 12
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 19
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 17,
                                  "column": 12
                                },
                                "end": {
                                  "line": 17,
                                  "column": 19
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 9
                              },
                              "end": {
                                "line": 17,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Float",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 17,
                                      "column": 24
                                    },
                                    "end": {
                                      "line": 17,
                                      "column": 29
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 24
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 30
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 17,
                                  "column": 24
                                },
                                "end": {
                                  "line": 17,
                                  "column": 30
                                }
                              }
                            },
                            "arguments": [
                              {
                                "type": "NumberLiteral",
                                "value": 6,
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 30
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 31
                                  }
                                }
                              },
                              {
                                "type": "StringLiteral",
                                "value": "3",
                                "loc": {
                                  "start": {
                                    "line": 17,
                                    "column": 32
                                  },
                                  "end": {
                                    "line": 17,
                                    "column": 35
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 17,
                                "column": 20
                              },
                              "end": {
                                "line": 17,
                                "column": 37
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 17,
                              "column": 9
                            },
                            "end": {
                              "line": 17,
                              "column": 37
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 17,
                          "column": 5
                        },
                        "end": {
                          "line": 17,
                          "column": 37
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 24
                    },
                    "end": {
                      "line": 18,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 14
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 14
                },
                "end": {
                  "line": 18,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 1
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 19,
      "column": 1
    }
  }
}
TypeError: No matching construct signature for std.core.Float(int, string) [validate_signatures_throw_type_error_more_param.ets:17:20]
