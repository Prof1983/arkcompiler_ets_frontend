{
  "type": "Program",
  "statements": [
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "my_type",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 6
          },
          "end": {
            "line": 16,
            "column": 13
          }
        }
      },
      "typeAnnotation": {
        "type": "ETSTypeReference",
        "part": {
          "type": "ETSTypeReferencePart",
          "name": {
            "type": "Identifier",
            "name": "T",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 19
              },
              "end": {
                "line": 16,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 19
            },
            "end": {
              "line": 16,
              "column": 21
            }
          }
        },
        "loc": {
          "start": {
            "line": 16,
            "column": 19
          },
          "end": {
            "line": 16,
            "column": 21
          }
        }
      },
      "typeParameters": {
        "type": "TSTypeParameterDeclaration",
        "params": [
          {
            "type": "TSTypeParameter",
            "name": {
              "type": "Identifier",
              "name": "T",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 14
                },
                "end": {
                  "line": 16,
                  "column": 15
                }
              }
            },
            "loc": {
              "start": {
                "line": 16,
                "column": 14
              },
              "end": {
                "line": 16,
                "column": 16
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 13
          },
          "end": {
            "line": 16,
            "column": 16
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 21
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 10
                },
                "end": {
                  "line": 19,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 10
                    },
                    "end": {
                      "line": 19,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 18
                    },
                    "end": {
                      "line": 19,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "my_type",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 20,
                                      "column": 12
                                    },
                                    "end": {
                                      "line": 20,
                                      "column": 19
                                    }
                                  }
                                },
                                "typeParams": {
                                  "type": "TSTypeParameterInstantiation",
                                  "params": [
                                    {
                                      "type": "ETSTypeReference",
                                      "part": {
                                        "type": "ETSTypeReferencePart",
                                        "name": {
                                          "type": "Identifier",
                                          "name": "Int",
                                          "decorators": [],
                                          "loc": {
                                            "start": {
                                              "line": 20,
                                              "column": 20
                                            },
                                            "end": {
                                              "line": 20,
                                              "column": 23
                                            }
                                          }
                                        },
                                        "loc": {
                                          "start": {
                                            "line": 20,
                                            "column": 20
                                          },
                                          "end": {
                                            "line": 20,
                                            "column": 24
                                          }
                                        }
                                      },
                                      "loc": {
                                        "start": {
                                          "line": 20,
                                          "column": 20
                                        },
                                        "end": {
                                          "line": 20,
                                          "column": 24
                                        }
                                      }
                                    },
                                    {
                                      "type": "ETSTypeReference",
                                      "part": {
                                        "type": "ETSTypeReferencePart",
                                        "name": {
                                          "type": "Identifier",
                                          "name": "Double",
                                          "decorators": [],
                                          "loc": {
                                            "start": {
                                              "line": 20,
                                              "column": 25
                                            },
                                            "end": {
                                              "line": 20,
                                              "column": 31
                                            }
                                          }
                                        },
                                        "loc": {
                                          "start": {
                                            "line": 20,
                                            "column": 25
                                          },
                                          "end": {
                                            "line": 20,
                                            "column": 32
                                          }
                                        }
                                      },
                                      "loc": {
                                        "start": {
                                          "line": 20,
                                          "column": 25
                                        },
                                        "end": {
                                          "line": 20,
                                          "column": 32
                                        }
                                      }
                                    }
                                  ],
                                  "loc": {
                                    "start": {
                                      "line": 20,
                                      "column": 19
                                    },
                                    "end": {
                                      "line": 20,
                                      "column": 32
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 20,
                                    "column": 12
                                  },
                                  "end": {
                                    "line": 20,
                                    "column": 34
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 20,
                                  "column": 12
                                },
                                "end": {
                                  "line": 20,
                                  "column": 34
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 20,
                                "column": 9
                              },
                              "end": {
                                "line": 20,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Int",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 20,
                                      "column": 39
                                    },
                                    "end": {
                                      "line": 20,
                                      "column": 42
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 20,
                                    "column": 39
                                  },
                                  "end": {
                                    "line": 20,
                                    "column": 43
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 20,
                                  "column": 39
                                },
                                "end": {
                                  "line": 20,
                                  "column": 43
                                }
                              }
                            },
                            "arguments": [],
                            "loc": {
                              "start": {
                                "line": 20,
                                "column": 35
                              },
                              "end": {
                                "line": 20,
                                "column": 45
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 20,
                              "column": 9
                            },
                            "end": {
                              "line": 20,
                              "column": 45
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 5
                        },
                        "end": {
                          "line": 20,
                          "column": 45
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 23
                    },
                    "end": {
                      "line": 21,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 19,
                    "column": 14
                  },
                  "end": {
                    "line": 21,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 19,
                  "column": 14
                },
                "end": {
                  "line": 21,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 19,
                "column": 1
              },
              "end": {
                "line": 21,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 22,
      "column": 1
    }
  }
}
TypeError: Wrong number of type parameters for generic type alias [generic_typealias_4_neg.ets:20:19]
