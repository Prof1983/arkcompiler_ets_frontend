{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 15,
              "column": 7
            },
            "end": {
              "line": 15,
              "column": 8
            }
          }
        },
        "typeParameters": {
          "type": "TSTypeParameterDeclaration",
          "params": [
            {
              "type": "TSTypeParameter",
              "name": {
                "type": "Identifier",
                "name": "T",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 15,
                    "column": 9
                  },
                  "end": {
                    "line": 15,
                    "column": 10
                  }
                }
              },
              "constraint": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "A",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 15,
                        "column": 19
                      },
                      "end": {
                        "line": 15,
                        "column": 20
                      }
                    }
                  },
                  "typeParams": {
                    "type": "TSTypeParameterInstantiation",
                    "params": [
                      {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "A",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 15,
                                "column": 21
                              },
                              "end": {
                                "line": 15,
                                "column": 22
                              }
                            }
                          },
                          "typeParams": {
                            "type": "TSTypeParameterInstantiation",
                            "params": [
                              {
                                "type": "ETSTypeReference",
                                "part": {
                                  "type": "ETSTypeReferencePart",
                                  "name": {
                                    "type": "Identifier",
                                    "name": "T",
                                    "decorators": [],
                                    "loc": {
                                      "start": {
                                        "line": 15,
                                        "column": 23
                                      },
                                      "end": {
                                        "line": 15,
                                        "column": 24
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 15,
                                      "column": 23
                                    },
                                    "end": {
                                      "line": 15,
                                      "column": 27
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 15,
                                    "column": 23
                                  },
                                  "end": {
                                    "line": 15,
                                    "column": 27
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 15,
                                "column": 22
                              },
                              "end": {
                                "line": 15,
                                "column": 27
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 15,
                              "column": 21
                            },
                            "end": {
                              "line": 15,
                              "column": 27
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 15,
                            "column": 21
                          },
                          "end": {
                            "line": 15,
                            "column": 27
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 15,
                        "column": 20
                      },
                      "end": {
                        "line": 15,
                        "column": 27
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 15,
                      "column": 19
                    },
                    "end": {
                      "line": 15,
                      "column": 27
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 15,
                    "column": 19
                  },
                  "end": {
                    "line": 15,
                    "column": 27
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 15,
                  "column": 9
                },
                "end": {
                  "line": 15,
                  "column": 27
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 15,
              "column": 8
            },
            "end": {
              "line": 15,
              "column": 27
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 15,
                "column": 29
              },
              "end": {
                "line": 15,
                "column": 29
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 15,
            "column": 27
          },
          "end": {
            "line": 15,
            "column": 29
          }
        }
      },
      "loc": {
        "start": {
          "line": 15,
          "column": 1
        },
        "end": {
          "line": 15,
          "column": 29
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "C",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "A",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 17
                },
                "end": {
                  "line": 16,
                  "column": 18
                }
              }
            },
            "typeParams": {
              "type": "TSTypeParameterInstantiation",
              "params": [
                {
                  "type": "ETSTypeReference",
                  "part": {
                    "type": "ETSTypeReferencePart",
                    "name": {
                      "type": "Identifier",
                      "name": "A",
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 19
                        },
                        "end": {
                          "line": 16,
                          "column": 20
                        }
                      }
                    },
                    "typeParams": {
                      "type": "TSTypeParameterInstantiation",
                      "params": [
                        {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "C",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 16,
                                  "column": 21
                                },
                                "end": {
                                  "line": 16,
                                  "column": 22
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 16,
                                "column": 21
                              },
                              "end": {
                                "line": 16,
                                "column": 24
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 16,
                              "column": 21
                            },
                            "end": {
                              "line": 16,
                              "column": 24
                            }
                          }
                        }
                      ],
                      "loc": {
                        "start": {
                          "line": 16,
                          "column": 20
                        },
                        "end": {
                          "line": 16,
                          "column": 24
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 16,
                        "column": 19
                      },
                      "end": {
                        "line": 16,
                        "column": 24
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 19
                    },
                    "end": {
                      "line": 16,
                      "column": 24
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 18
                },
                "end": {
                  "line": 16,
                  "column": 24
                }
              }
            },
            "loc": {
              "start": {
                "line": 16,
                "column": 17
              },
              "end": {
                "line": 16,
                "column": 25
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 17
            },
            "end": {
              "line": 16,
              "column": 25
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 26
              },
              "end": {
                "line": 16,
                "column": 26
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 24
          },
          "end": {
            "line": 16,
            "column": 26
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 26
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 17,
      "column": 1
    }
  }
}
TypeError: Type A<T> is not assignable to constraint type A<A<A<T>>> [recursive_class_neg.ets:15:20]
