{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 17
          },
          "end": {
            "line": 16,
            "column": 19
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "iface",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 11
          },
          "end": {
            "line": 16,
            "column": 16
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 18,
          "column": 9
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 10
                },
                "end": {
                  "line": 18,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 10
                    },
                    "end": {
                      "line": 18,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 18
                    },
                    "end": {
                      "line": 18,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "ETSNewArrayInstanceExpression",
                        "typeReference": {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "iface",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 19,
                                  "column": 7
                                },
                                "end": {
                                  "line": 19,
                                  "column": 12
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 7
                              },
                              "end": {
                                "line": 19,
                                "column": 13
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 7
                            },
                            "end": {
                              "line": 19,
                              "column": 13
                            }
                          }
                        },
                        "dimension": {
                          "type": "NumberLiteral",
                          "value": 5,
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 13
                            },
                            "end": {
                              "line": 19,
                              "column": 14
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 3
                          },
                          "end": {
                            "line": 19,
                            "column": 15
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 3
                        },
                        "end": {
                          "line": 19,
                          "column": 16
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 23
                    },
                    "end": {
                      "line": 20,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 14
                  },
                  "end": {
                    "line": 20,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 14
                },
                "end": {
                  "line": 20,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 1
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 21,
      "column": 1
    }
  }
}
TypeError: Cannot use array creation expression with abstract classes and interfaces. [instantied_interface_with_array_creation_expression.ets:19:3]
