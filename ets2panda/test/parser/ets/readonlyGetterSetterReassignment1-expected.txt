{
  "type": "Program",
  "statements": [
    {
      "type": "TSInterfaceDeclaration",
      "body": {
        "type": "TSInterfaceBody",
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 17
                    },
                    "end": {
                      "line": 17,
                      "column": 24
                    }
                  }
                },
                "declare": true,
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 5
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 13
          },
          "end": {
            "line": 18,
            "column": 2
          }
        }
      },
      "id": {
        "type": "Identifier",
        "name": "I",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 11
          },
          "end": {
            "line": 16,
            "column": 12
          }
        }
      },
      "extends": [],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 20,
          "column": 6
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 20,
              "column": 7
            },
            "end": {
              "line": 20,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [
          {
            "type": "TSClassImplements",
            "expression": {
              "type": "ETSTypeReference",
              "part": {
                "type": "ETSTypeReferencePart",
                "name": {
                  "type": "Identifier",
                  "name": "I",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 20
                    },
                    "end": {
                      "line": 20,
                      "column": 21
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 20
                  },
                  "end": {
                    "line": 20,
                    "column": 23
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 20,
                  "column": 20
                },
                "end": {
                  "line": 20,
                  "column": 23
                }
              }
            },
            "loc": {
              "start": {
                "line": 20,
                "column": 20
              },
              "end": {
                "line": 20,
                "column": 23
              }
            }
          }
        ],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 21,
                  "column": 14
                },
                "end": {
                  "line": 21,
                  "column": 15
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": true,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 21,
                  "column": 17
                },
                "end": {
                  "line": 21,
                  "column": 24
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 21,
                "column": 14
              },
              "end": {
                "line": 21,
                "column": 24
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 23,
                                "column": 9
                              },
                              "end": {
                                "line": 23,
                                "column": 13
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 23,
                                "column": 14
                              },
                              "end": {
                                "line": 23,
                                "column": 15
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 23,
                              "column": 9
                            },
                            "end": {
                              "line": 23,
                              "column": 15
                            }
                          }
                        },
                        "right": {
                          "type": "BooleanLiteral",
                          "value": false,
                          "loc": {
                            "start": {
                              "line": 23,
                              "column": 18
                            },
                            "end": {
                              "line": 23,
                              "column": 23
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 23,
                            "column": 9
                          },
                          "end": {
                            "line": 23,
                            "column": 23
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 23,
                          "column": 9
                        },
                        "end": {
                          "line": 23,
                          "column": 23
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 22,
                      "column": 19
                    },
                    "end": {
                      "line": 24,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 22,
                    "column": 16
                  },
                  "end": {
                    "line": 24,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 22,
                  "column": 16
                },
                "end": {
                  "line": 24,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 22,
                "column": 5
              },
              "end": {
                "line": 24,
                "column": 6
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 20,
            "column": 22
          },
          "end": {
            "line": 25,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 20,
          "column": 1
        },
        "end": {
          "line": 25,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 27,
              "column": 7
            },
            "end": {
              "line": 27,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "A",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 27,
                  "column": 17
                },
                "end": {
                  "line": 27,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 27,
                "column": 17
              },
              "end": {
                "line": 27,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 27,
              "column": 17
            },
            "end": {
              "line": 27,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "AssignmentExpression",
                        "operator": "=",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "ThisExpression",
                            "loc": {
                              "start": {
                                "line": 29,
                                "column": 9
                              },
                              "end": {
                                "line": 29,
                                "column": 13
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 29,
                                "column": 14
                              },
                              "end": {
                                "line": 29,
                                "column": 15
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 29,
                              "column": 9
                            },
                            "end": {
                              "line": 29,
                              "column": 15
                            }
                          }
                        },
                        "right": {
                          "type": "BooleanLiteral",
                          "value": true,
                          "loc": {
                            "start": {
                              "line": 29,
                              "column": 18
                            },
                            "end": {
                              "line": 29,
                              "column": 22
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 29,
                            "column": 9
                          },
                          "end": {
                            "line": 29,
                            "column": 22
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 29,
                          "column": 9
                        },
                        "end": {
                          "line": 29,
                          "column": 23
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 28,
                      "column": 19
                    },
                    "end": {
                      "line": 30,
                      "column": 6
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 28,
                    "column": 16
                  },
                  "end": {
                    "line": 30,
                    "column": 6
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 28,
                  "column": 16
                },
                "end": {
                  "line": 30,
                  "column": 6
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 28,
                "column": 5
              },
              "end": {
                "line": 30,
                "column": 6
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 27,
            "column": 19
          },
          "end": {
            "line": 31,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 27,
          "column": 1
        },
        "end": {
          "line": 31,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 33,
                  "column": 10
                },
                "end": {
                  "line": 33,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 33,
                      "column": 10
                    },
                    "end": {
                      "line": 33,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 34,
                                "column": 9
                              },
                              "end": {
                                "line": 34,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "A",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 34,
                                      "column": 17
                                    },
                                    "end": {
                                      "line": 34,
                                      "column": 18
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 34,
                                    "column": 17
                                  },
                                  "end": {
                                    "line": 34,
                                    "column": 19
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 34,
                                  "column": 17
                                },
                                "end": {
                                  "line": 34,
                                  "column": 19
                                }
                              }
                            },
                            "arguments": [],
                            "loc": {
                              "start": {
                                "line": 34,
                                "column": 13
                              },
                              "end": {
                                "line": 34,
                                "column": 21
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 34,
                              "column": 9
                            },
                            "end": {
                              "line": 34,
                              "column": 21
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 34,
                          "column": 5
                        },
                        "end": {
                          "line": 34,
                          "column": 21
                        }
                      }
                    },
                    {
                      "type": "AssertStatement",
                      "test": {
                        "type": "BinaryExpression",
                        "operator": "==",
                        "left": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 35,
                                "column": 12
                              },
                              "end": {
                                "line": 35,
                                "column": 13
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 35,
                                "column": 14
                              },
                              "end": {
                                "line": 35,
                                "column": 15
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 35,
                              "column": 12
                            },
                            "end": {
                              "line": 35,
                              "column": 15
                            }
                          }
                        },
                        "right": {
                          "type": "BooleanLiteral",
                          "value": false,
                          "loc": {
                            "start": {
                              "line": 35,
                              "column": 19
                            },
                            "end": {
                              "line": 35,
                              "column": 24
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 35,
                            "column": 11
                          },
                          "end": {
                            "line": 35,
                            "column": 25
                          }
                        }
                      },
                      "second": null,
                      "loc": {
                        "start": {
                          "line": 35,
                          "column": 5
                        },
                        "end": {
                          "line": 35,
                          "column": 25
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 33,
                      "column": 17
                    },
                    "end": {
                      "line": 36,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 33,
                    "column": 14
                  },
                  "end": {
                    "line": 36,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 33,
                  "column": 14
                },
                "end": {
                  "line": 36,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 33,
                "column": 1
              },
              "end": {
                "line": 36,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 37,
      "column": 1
    }
  }
}
TypeError: Cannot assign to this property because it is readonly. [readonlyGetterSetterReassignment1.ets:29:14]
