{
  "type": "Program",
  "statements": [
    {
      "type": "ImportDeclaration",
      "source": {
        "type": "StringLiteral",
        "value": "./main__foo1",
        "loc": {
          "start": {
            "line": 16,
            "column": 17
          },
          "end": {
            "line": 16,
            "column": 31
          }
        }
      },
      "specifiers": [
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "A",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 10
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "A",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 10
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 9
            },
            "end": {
              "line": 16,
              "column": 10
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 31
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 17
                },
                "end": {
                  "line": 18,
                  "column": 20
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 17
                    },
                    "end": {
                      "line": 18,
                      "column": 20
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "a",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "A",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 24
                              },
                              "end": {
                                "line": 18,
                                "column": 25
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 24
                            },
                            "end": {
                              "line": 18,
                              "column": 26
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 24
                          },
                          "end": {
                            "line": 18,
                            "column": 26
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 21
                        },
                        "end": {
                          "line": 18,
                          "column": 26
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 21
                      },
                      "end": {
                        "line": 18,
                        "column": 26
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 28
                    },
                    "end": {
                      "line": 18,
                      "column": 31
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "MemberExpression",
                          "object": {
                            "type": "Identifier",
                            "name": "console",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 5
                              },
                              "end": {
                                "line": 19,
                                "column": 12
                              }
                            }
                          },
                          "property": {
                            "type": "Identifier",
                            "name": "log",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 13
                              },
                              "end": {
                                "line": 19,
                                "column": 16
                              }
                            }
                          },
                          "computed": false,
                          "optional": false,
                          "loc": {
                            "start": {
                              "line": 19,
                              "column": 5
                            },
                            "end": {
                              "line": 19,
                              "column": 16
                            }
                          }
                        },
                        "arguments": [
                          {
                            "type": "StringLiteral",
                            "value": "in foo",
                            "loc": {
                              "start": {
                                "line": 19,
                                "column": 17
                              },
                              "end": {
                                "line": 19,
                                "column": 25
                              }
                            }
                          }
                        ],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 5
                          },
                          "end": {
                            "line": 19,
                            "column": 26
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 5
                        },
                        "end": {
                          "line": 19,
                          "column": 27
                        }
                      }
                    },
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "NumberLiteral",
                        "value": 0,
                        "loc": {
                          "start": {
                            "line": 20,
                            "column": 12
                          },
                          "end": {
                            "line": 20,
                            "column": 13
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 5
                        },
                        "end": {
                          "line": 20,
                          "column": 14
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 32
                    },
                    "end": {
                      "line": 21,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 20
                  },
                  "end": {
                    "line": 21,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 20
                },
                "end": {
                  "line": 21,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 8
              },
              "end": {
                "line": 21,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 22,
      "column": 1
    }
  }
}
