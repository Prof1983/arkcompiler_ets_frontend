{
  "type": "Program",
  "statements": [
    {
      "type": "ImportDeclaration",
      "source": {
        "type": "StringLiteral",
        "value": "./foo3__module",
        "loc": {
          "start": {
            "line": 16,
            "column": 24
          },
          "end": {
            "line": 16,
            "column": 40
          }
        }
      },
      "specifiers": [
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "foo_in_3",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 17
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "foo_in_3",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 9
              },
              "end": {
                "line": 16,
                "column": 17
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 9
            },
            "end": {
              "line": 16,
              "column": 17
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 40
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 18,
              "column": 14
            },
            "end": {
              "line": 18,
              "column": 15
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 5
                },
                "end": {
                  "line": 19,
                  "column": 6
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSPrimitiveType",
              "loc": {
                "start": {
                  "line": 19,
                  "column": 8
                },
                "end": {
                  "line": 19,
                  "column": 11
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 19,
                "column": 5
              },
              "end": {
                "line": 19,
                "column": 11
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 20,
                "column": 2
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 18,
            "column": 16
          },
          "end": {
            "line": 20,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 18,
          "column": 8
        },
        "end": {
          "line": 20,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 23,
                  "column": 10
                },
                "end": {
                  "line": 23,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 23,
                      "column": 10
                    },
                    "end": {
                      "line": 23,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 23,
                      "column": 19
                    },
                    "end": {
                      "line": 23,
                      "column": 23
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 24,
                                "column": 9
                              },
                              "end": {
                                "line": 24,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "ETSNewClassInstanceExpression",
                            "typeReference": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "A",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 24,
                                      "column": 17
                                    },
                                    "end": {
                                      "line": 24,
                                      "column": 18
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 24,
                                    "column": 17
                                  },
                                  "end": {
                                    "line": 24,
                                    "column": 19
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 24,
                                  "column": 17
                                },
                                "end": {
                                  "line": 24,
                                  "column": 19
                                }
                              }
                            },
                            "arguments": [],
                            "loc": {
                              "start": {
                                "line": 24,
                                "column": 13
                              },
                              "end": {
                                "line": 24,
                                "column": 21
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 24,
                              "column": 9
                            },
                            "end": {
                              "line": 24,
                              "column": 21
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 24,
                          "column": 5
                        },
                        "end": {
                          "line": 24,
                          "column": 21
                        }
                      }
                    },
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "Identifier",
                          "name": "foo_in_3",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 25,
                              "column": 5
                            },
                            "end": {
                              "line": 25,
                              "column": 13
                            }
                          }
                        },
                        "arguments": [],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 25,
                            "column": 5
                          },
                          "end": {
                            "line": 25,
                            "column": 15
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 25,
                          "column": 5
                        },
                        "end": {
                          "line": 25,
                          "column": 16
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 23,
                      "column": 24
                    },
                    "end": {
                      "line": 26,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 23,
                    "column": 14
                  },
                  "end": {
                    "line": 26,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 23,
                  "column": 14
                },
                "end": {
                  "line": 26,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 23,
                "column": 1
              },
              "end": {
                "line": 26,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 27,
      "column": 1
    }
  }
}
