{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "C",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 11
              },
              "end": {
                "line": 16,
                "column": 11
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 9
          },
          "end": {
            "line": 16,
            "column": 11
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 11
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "alma",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 10
                },
                "end": {
                  "line": 18,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "alma",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 10
                    },
                    "end": {
                      "line": 18,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "x",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "Object",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 18
                              },
                              "end": {
                                "line": 18,
                                "column": 24
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 18
                            },
                            "end": {
                              "line": 18,
                              "column": 25
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 18
                          },
                          "end": {
                            "line": 18,
                            "column": 25
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 15
                        },
                        "end": {
                          "line": 18,
                          "column": 25
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 15
                      },
                      "end": {
                        "line": 18,
                        "column": 25
                      }
                    }
                  }
                ],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "Identifier",
                        "name": "x",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 12
                          },
                          "end": {
                            "line": 19,
                            "column": 13
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 5
                        },
                        "end": {
                          "line": 19,
                          "column": 14
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 26
                    },
                    "end": {
                      "line": 20,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 14
                  },
                  "end": {
                    "line": 20,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 14
                },
                "end": {
                  "line": 20,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 1
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 22,
                  "column": 10
                },
                "end": {
                  "line": 22,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 22,
                      "column": 10
                    },
                    "end": {
                      "line": 22,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 22,
                      "column": 18
                    },
                    "end": {
                      "line": 22,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "CallExpression",
                        "callee": {
                          "type": "Identifier",
                          "name": "alma",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 23,
                              "column": 5
                            },
                            "end": {
                              "line": 23,
                              "column": 9
                            }
                          }
                        },
                        "arguments": [
                          {
                            "type": "Identifier",
                            "name": "C",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 23,
                                "column": 10
                              },
                              "end": {
                                "line": 23,
                                "column": 11
                              }
                            }
                          }
                        ],
                        "optional": false,
                        "loc": {
                          "start": {
                            "line": 23,
                            "column": 5
                          },
                          "end": {
                            "line": 23,
                            "column": 12
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 23,
                          "column": 5
                        },
                        "end": {
                          "line": 23,
                          "column": 13
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 22,
                      "column": 23
                    },
                    "end": {
                      "line": 24,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 22,
                    "column": 14
                  },
                  "end": {
                    "line": 24,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 22,
                  "column": 14
                },
                "end": {
                  "line": 24,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 22,
                "column": 1
              },
              "end": {
                "line": 24,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 25,
      "column": 1
    }
  }
}
TypeError: Class or interface 'C' cannot be used as object [classAsFunctionParam.ets:23:10]
