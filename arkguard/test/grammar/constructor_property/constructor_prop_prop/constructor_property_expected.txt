/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
namespace ts {
    let b: {
        g(o: c): number;
        h(o: c, v: number): void;
    };
    class c {
        x: number;
        constructor(k: number) {
            this.x = k;
        }
        g() {
            return this.x;
        }
        i() {
            b = {
                g(j) { return j.x; },
                h(h, i) { h.x = i; }
            };
        }
    }
    ;
    class d {
        constructor(public a: c, private j: number = 1, protected k: string = '', readonly l: number = 2) {
            const g = b.g(a); // ok
            b.h(a, g + 1); // ok
        }
    }
    ;
    const e = new c(41);
    e.i();
    const f = new d(e);
    e.g();
}
